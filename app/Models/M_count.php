<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_count extends Model
{
    protected $table="countnotification";// nombre de la tabla
    protected $primaryKey="id";
    protected $fillable = [
        'tipo_f_incodusu',    
        'number',    
    ];

  
}
