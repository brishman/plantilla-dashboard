<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_pers extends Model
{
    protected $with =['m_usua']; // relacion de tablas 
    protected $table="m_pers";// nombre de la tabla
    protected $primaryKey="id";
    protected $fillable = [
        'usua_f_incodusu',  // codigo de la tabla m_usua        
        'pers_chnomper',    //nombre pèrsona
        'pers_chapeper',          //apellido persona
        'pers_chdocide',         //documento identidad
        'pers_chcelper',        //celular persona
        'pers_chdirper'         //direccion persona
    ];

    public function m_usua()
    {
        return $this->belongsTo(M_usua::class,'usua_f_incodusu','id');
    }   
}
