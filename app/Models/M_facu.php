<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_facu extends Model
{
    protected $table="m_facu";// nombre de la tabla
    protected $primaryKey="id";
    protected $hidden = ["created_at", "updated_at"];
    protected $fillable = [
        'facu_chnomfac',    //nombre facultad     
        'facu_boestfac'	//estado facultad
          
    ];
}
