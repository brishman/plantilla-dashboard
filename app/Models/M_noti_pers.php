<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_noti_pers extends Model
{
    protected $with =['tipo_usuario','semestre','facultad','nivel_usuario']; // relacion de tablas 
    protected $table="m_noti_pers";// nombre de la tabla
    protected $primaryKey="id";
    protected $fillable = [
        'seme_f_incodaca',  // semestre         
        'facu_f_incodfac',  //facultad
        'tipo_f_incodusu',  //tipo usuario
        'nive_f_incodniv', //Nivel usuario
        'title',   //titulo
        'body',   //Cuerpo de notificacion
        'date',   //fecha,
        'image',   //imagen
        'number_student', //Numero,
        'number_teacher'

    ];

    public function tipo_usuario()
    {
        return $this->belongsTo(S_tipo_usua::class,'tipo_f_incodusu','id');
    }   
    public function nivel_usuario()
    {
        return $this->belongsTo(S_nive::class,'nive_f_incodniv','id');
    }
    public function semestre()
    {
        return $this->belongsTo(S_seme_acad::class,'nive_f_incodniv','id');
    }   
    
    public function facultad()
    {
        return $this->belongsTo(M_facu::class,'facu_f_incodfac','id');
    }   
   
}
