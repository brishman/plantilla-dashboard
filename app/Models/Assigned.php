<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assigned extends Model
{
    protected $with =['role'];
    protected $table="assigned_roles";
    protected $fillable = [
        'user_id', 'role_id'
    ];
    protected $hidden = ["created_at", "updated_at"];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id','id');
    }
}