<?php

namespace App\Models;
use transformers\UserTransformer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $with =['tipo_usuario','nivel_usuario','facultad']; // relacion de tablas 

    protected $table="users";
    protected $primaryKey="id";
    
    protected $fillable = [
        'tipo_f_incodusu',  // codigo de la tabla s_tipo_usua     
        'nive_f_incodniv',  // codigo de la tabla s_nive   
        'facu_f_incodfac',  // codigo de la tabla m_facu    
        'name', //Nombres y Apellidos
        'email', //email
        'password',//password
        'token',
        'usua_boestusu',
        'new_password',
        'pers_chdocide',         //documento identidad
        'pers_chcelper',        //celular persona
        'pers_chdirper'        //direccion persona
    ];

    
    protected $hidden = ["created_at", "updated_at","password","remember_token"];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function tipo_usuario()
    {
        return $this->belongsTo(S_tipo_usua::class,'tipo_f_incodusu','id');
    }   

    public function nivel_usuario()
    {
        return $this->belongsTo(S_nive::class,'nive_f_incodniv','id');
    }   
    
    public function facultad()
    {
        return $this->belongsTo(M_facu::class,'facu_f_incodfac','id');
    }   

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'assigned_roles');
    }

    public function hasRoles(array $roles)
    {
       // return $this->roles->pluck('name')->intersect($roles)->count();
       foreach ($roles as $role) {
          
        foreach ($this->roles  as $userRole) {
            if($userRole->name===$role){
                return true;
            }
        }
       }
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
