<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class S_nive extends Model
{
    protected $table="s_nive";// nombre de la tabla
    protected $primaryKey="id";
    protected $hidden = ["created_at", "updated_at"];
    protected $fillable = [
        'nive_chnomniv',    //nombre nivel         
    ];
}
