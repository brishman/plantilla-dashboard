<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_oper_noti extends Model
{
    protected $with =['s_tipo_usua','m_facu','m_noti_pers']; // relacion de tablas 
    protected $table="v_oper_noti";// nombre de la tabla
    protected $primaryKey="id";
    protected $fillable = [
        'tipo_f_incodusu',  // codigo de la tabla s_tipo_usua    
        'facu_f_incodfac',  // codigo de la tabla m_facu 
        'noti_f_incodper'  // codigo de la tabla m_noti_pers  
                     
    ];

    public function s_tipo_usua()
    {
        return $this->belongsTo(S_tipo_usua::class,'tipo_f_incodusu','id');
    }   

    public function m_facu()
    {
        return $this->belongsTo(S_facu::class,'facu_f_incodfac','id');
    }   
    
    public function m_noti_pers()
    {
        return $this->belongsTo(M_noti_pers::class,'noti_f_incodper','id');
    }   
}
