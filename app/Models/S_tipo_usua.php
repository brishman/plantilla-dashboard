<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class S_tipo_usua extends Model
{
    protected $table="s_tipo_usua";// nombre de la tabla
    protected $primaryKey="id";
    protected $hidden = ["created_at", "updated_at"];
    protected $fillable = [
        'tipo_chnomusu',       // nombre tipo usuario      
    ];
}
