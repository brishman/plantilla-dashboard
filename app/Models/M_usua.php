<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_usua extends Model
{
    protected $with =['s_tipo_usua','s_nive','m_facu']; // relacion de tablas 
    protected $table="m_usua";// nombre de la tabla
    protected $primaryKey="id";
    protected $fillable = [
        'tipo_f_incodusu',  // codigo de la tabla s_tipo_usua     
        'nive_f_incodniv',  // codigo de la tabla s_nive   
        'facu_f_incodfac',  // codigo de la tabla m_facu      
        'usua_chlogusu',    // logueo usuario
        'usua_chpasusu',    //pass usuario
        'usua_boestusu'    //estado usuario
    ];

    public function s_tipo_usua()
    {
        return $this->belongsTo(S_tipo_usua::class,'tipo_f_incodusu','id');
    }   

    public function s_nive()
    {
        return $this->belongsTo(S_nive::class,'nive_f_incodniv','id');
    }   
    
    public function m_facu()
    {
        return $this->belongsTo(M_facu::class,'facu_f_incodfac','id');
    }   
}
