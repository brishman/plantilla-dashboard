<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class S_seme_acad extends Model
{
    protected $table="s_seme_acad";// nombre de la tabla
    protected $primaryKey="id";
    protected $hidden = ["created_at", "updated_at"];
    protected $fillable = [
        'seme_chnomaca',  //nombre academico (semestre academico)
        'seme_boestaca'               //estado semenestre
    ];
}
