<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests\M_persRequest;
use App\Http\Resources\M_persCollection;
use App\Http\Resources\M_persResource;
use App\Models\M_pers;
use App\Models\M_usua;
use Illuminate\Http\Request;

class M_persController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.m_pers.index');
    }

    public function columns()//buscador x campo
    {
        return [
            'id' => 'Código',
            'name1' => 'Buscador1',
            'name2' => 'Buscador2'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = M_pers::where($request->column, 'like', "%{$request->value}%")->where('state','1')->orderBy('name');//para ordenar

        return new M_persCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new M_persResource(M_pers::findOrFail($id));
        return $record;
    }

    public function store(M_persRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $m_pers = M_pers::firstOrNew(['id' => $id]);
        $m_pers->fill($request->all());
        $m_pers->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$m_pers
        ];
    }

    public function destroy($id)//Eliminar
    {
        $m_pers = M_pers::findOrFail($id);
        $m_pers->state='0';
        $m_pers->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
