<?php

namespace App\Http\Controllers;
use App\Http\Requests\S_niveRequest;
use App\Http\Resources\S_niveCollection;
use App\Http\Resources\S_niveResource;
use App\Models\S_nive;
use Illuminate\Http\Request;

class S_niveController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.s_nive.index');
    }

    public function columns()//buscador x campo
    {
        return [
            'id'    => 'ID',
            'nive_chnomniv' => 'Nombre'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = S_nive::where($request->column, 'like', "%{$request->value}%")->orderBy('nive_chnomniv');//para ordenar

        return new S_niveCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new S_niveResource(S_nive::findOrFail($id));
        return $record;
    }

    public function store(S_niveRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $s_nive = S_nive::firstOrNew(['id' => $id]);
        $s_nive->fill($request->all());
        $s_nive->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$s_nive
        ];
    }

    public function destroy($id)//Eliminar
    {
        $s_nive = S_nive::destroy($id);
        // $s_nive->state='0';
        // $s_nive->destroy($id);
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
