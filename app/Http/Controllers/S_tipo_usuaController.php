<?php

namespace App\Http\Controllers;
use App\Http\Requests\S_tipo_usuaRequest;
use App\Http\Resources\S_tipo_usuaCollection;
use App\Http\Resources\S_tipo_usuaResource;
use App\Models\S_tipo_usua;
use Illuminate\Http\Request;

class S_tipo_usuaController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.s_tipo_usua.index');
    }

    public function columns()//buscador x campo
    {
        return [
            'id' => 'Código',
            'tipo_chnomusu' => 'Buscador1',
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = S_tipo_usua::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar
        return new S_tipo_usuaCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new S_tipo_usuaResource(S_tipo_usua::findOrFail($id));
        return $record;
    }

    public function store(S_tipo_usuaRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $s_tipo_usua = S_tipo_usua::firstOrNew(['id' => $id]);
        $s_tipo_usua->fill($request->all());
        $s_tipo_usua->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$s_tipo_usua
        ];
    }

    public function destroy($id)//Eliminar
    {
        $s_tipo_usua = S_tipo_usua::findOrFail($id);
        $s_tipo_usua->state='0';
        $s_tipo_usua->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
