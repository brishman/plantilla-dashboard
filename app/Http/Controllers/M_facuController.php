<?php
namespace App\Http\Controllers;
use App\Http\Requests\M_facuRequest;
use App\Http\Resources\M_facuCollection;
use App\Http\Resources\M_facuResource;
use App\Models\M_facu;
use Illuminate\Http\Request;

class M_facuController extends Controller
{    

public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.m_facu.index');
    }

    public function columns()//buscador x campo 
    {
        return [
            'facu_chnomfac' => 'Nombre',
            'id' => 'Id'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = M_facu::where($request->column, 'like', "%{$request->value}%")->where('facu_boestfac', '!=','0')->orderBy('facu_chnomfac');//para ordenar
        // $records = M_facu::where($request->column, 'like', "%{$request->value}%")->orderBy('facu_chnomfac', 'desc');//para ordenar
        return new M_facuCollection($records->paginate(config('tenant.items_per_page')));
       
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new M_facuResource(M_facu::findOrFail($id));
        return $record;
    }

    public function store(M_facuRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $m_facu = M_facu::firstOrNew(['id' => $id]);
        $m_facu->fill($request->all());
        $m_facu->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    => $m_facu
        ];
    }

    public function destroy($id)//Eliminar
    {
        $m_facu = M_facu::findOrFail($id);
        $m_facu->facu_boestfac='0';
        $m_facu->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
