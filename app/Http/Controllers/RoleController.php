<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{
 
    public function index()
    {
        return view('funeraria.roles.index'); // nombre tabla
    }
    public function columns()//buscador
    {
        return [
            'name' => 'Nombre',
        ];
    }

    public function store(Roles $request)
    {
        $id = $request->input('id');
        $product =Rol::firstOrNew(['id' => $id]);
        $product->fill($request->all());
        $product->save();
        return [
            'success' => true,
            'message' => ($id)?'Modificado con éxito':'Registrado con éxito',
            'data'     =>$product
        ];
    }

    public function destroy($id)
    {
        
    }
}
