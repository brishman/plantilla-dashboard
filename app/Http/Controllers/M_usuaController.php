<?php

namespace App\Http\Controllers;
use App\Http\Requests\M_usuaRequest;
use App\Http\Resources\M_usuaCollection;
use App\Http\Resources\M_usuaResource;
use App\Models\M_usua;
use App\Models\S_tipo_usua ;
use App\Models\S_nive ;
use App\Models\M_facu   ;
use Illuminate\Http\Request;

class M_usuaController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.m_usua.index');
    }

    public function columns()//buscador x campo
    {
        return [
            'id' => 'Código',
            'name1' => 'Buscador1',
            'name2' => 'Buscador2'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = M_usua::where($request->column, 'like', "%{$request->value}%")->where('state','1')->orderBy('name');

        return new M_usuaCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new M_usuaResource(M_usua::findOrFail($id));
        return $record;
    }

    public function store(M_usuaRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $m_usua = M_usua::firstOrNew(['id' => $id]);
        $m_usua->fill($request->all());
        $m_usua->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$m_usua
        ];
    }

    public function destroy($id)//Eliminar
    {
        $m_usua = M_usua::findOrFail($id);
        $m_usua->state='0';
        $m_usua->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
