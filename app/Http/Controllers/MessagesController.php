<?php
namespace App\Http\Controllers;
use App\Models\M_facu;
use App\Models\M_noti_pers;
use App\Models\S_seme_acad;
use App\Models\S_tipo_usua;
use Illuminate\Http\Request;
use App\Events\NotificationStudentEvent;
use App\Http\Requests\M_noti_persRequest;

class MessagesController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.notifications.index');
    }
    public function student_messages($faculties,$semesters){
        $notificaciones = M_noti_pers::where('facu_f_incodfac', '=',$faculties)->where('seme_f_incodaca', '=',$semesters)->last();
        broadcast(new App\Events\NotificationStudentEvent("Hola desde mi pc"));
        return compact('notificaciones');
    }
}
