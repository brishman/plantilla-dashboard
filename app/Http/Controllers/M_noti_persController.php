<?php
namespace App\Http\Controllers;
use App\Models\M_facu;
use App\Models\M_noti_pers;
use App\Models\S_seme_acad;
use App\Models\S_tipo_usua;
use App\Models\M_count;
use Illuminate\Http\Request;
use App\Events\NotificationStudentEvent;
use App\Http\Requests\M_noti_persRequest;
use App\Http\Resources\M_noti_persResource;
use App\Http\Resources\M_noti_persCollection;
use DB;
use Barryvdh\DomPDF\Facade as PDF;

class M_noti_persController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.notifications.index');
    }
    public function exportar()//Llama al template Blade de laravel
    {
        return view('notificaciones.exports.index');
    }
    public function viewrecycle(){
        return view('notificaciones.recycle.index');
    }
    public function recycle(Request $request){
        
        if($request->seme_f_incodaca==null){
            $notificaciones=M_noti_pers::where('tipo_f_incodusu',$request->input('tipo_f_incodusu'))->where('facu_f_incodfac',$request->input('facu_f_incodfac'))->get();
        }else{
            $notificaciones=M_noti_pers::where('tipo_f_incodusu',$request->input('tipo_f_incodusu'))->where('facu_f_incodfac',$request->input('facu_f_incodfac'))->where('seme_f_incodaca',$request->input('seme_f_incodaca'))->get();
        }
 
        foreach ($notificaciones as $row) {
            $m_noti_pers = M_noti_pers::findOrFail($row->id);
            $m_noti_pers->delete(); 
        }
        return response()->json([
            'success' => true,
            'message' => 'Se Eliminó las notificaciones con exito..',
        ], 201);     
    }
    public function exportpdf($nive_f_incodniv,$tipo_f_incodusu,$facu_f_incodfac,$seme_f_incodaca){
        if($seme_f_incodaca=="0"){
            $notificaciones = M_noti_pers::where('nive_f_incodniv', $nive_f_incodniv)
            ->where('tipo_f_incodusu', $tipo_f_incodusu)
            ->where('facu_f_incodfac', $facu_f_incodfac)
            ->orderBy('id','asc')->get();
        }else{
            $notificaciones = M_noti_pers::where('nive_f_incodniv', $nive_f_incodniv)
            ->where('tipo_f_incodusu', $tipo_f_incodusu)
            ->where('facu_f_incodfac', $facu_f_incodfac)
            ->where('seme_f_incodaca', $seme_f_incodaca)
            ->orderBy('id','asc')->get();
        }
        return PDF::loadView('reports.report_notificaciones',['notificaciones'=>$notificaciones])->setPaper('A4')->stream();

    }
    public function columns()//buscador x campo
    {
        return [
            'id' => 'Código',
            'title' => 'Titulo de Notificación',
            'body' => 'Contenido de Notificación',
            'seme_f_incodaca' => 'Semestre',
            'facu_f_incodfac' => 'Facultad Academica'
          
        ];
    }
  public function record($id)//Selecccionar un Registro
    {
        $record = new M_noti_persResource(M_noti_pers::findOrFail($id));
        return $record;
    }
    public function records(Request $request)//Genera lista de registro para el grid
    { 
        //dd($request->column);
        $records = M_noti_pers::where($request->column, 'like', "%{$request->value}%");
        return new M_noti_persCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function contarstudentnotificaciones($tipo,$id){
        $sql_count=M_count::select(DB::raw('max(number) as number'))->where('nive_f_incodniv','1')->first();
        $cantidad=$sql_count->number+1;
        $count=M_count::findOrFail('1');
        $count->number=$cantidad;
        $count->save();
        $notiAlumnos=M_noti_pers::findOrFail($id);
        $notiAlumnos->number_student=$notiAlumnos->number_student+1;
        $notiAlumnos->save();
        return response()->json([
            'success' => true,
            'count'=> $cantidad,
            'messsage' => 'Se ha enviado con exito..',
        ], 201); 
    }
    public function contarnotificaciones($tipo,$id){
        $sql_count=M_count::select(DB::raw('max(number) as number'))->where('nive_f_incodniv','2')->first();
        $cantidad=$sql_count->number+1;
        $count=M_count::findOrFail($tipo);
        $count->number=$cantidad;
        $count->save();
        $notiAlumnos=M_noti_pers::findOrFail($id);
        $notiAlumnos->number_teacher=$notiAlumnos->number_teacher+1;
        $notiAlumnos->save();
        return response()->json([
            'success' => true,
            'count'=> $cantidad,
            'messsage' => 'Se ha enviado con exito..',
        ], 201); 
    }
    public function viewcountnotifications(){
        return view('notificaciones.notifications.viewcount');
    }
    public function countnotificacion(){
        $count_student=M_count::select(DB::raw('max(number) as number'))->where('nive_f_incodniv','1')->first();
        $count_docente=M_count::select(DB::raw('max(number) as number'))->where('nive_f_incodniv','2')->first();

        return response()->json([
            'success' => true,
            'student' => $count_student->number,
            'teacher' => $count_docente->number
        ], 201);  
    }

    public function notifications($facultad,$seme_f_incodaca)
    {
        $notifications = M_noti_pers::where('tipo_f_incodusu',"=",'1')
                                         ->where('seme_f_incodaca',$seme_f_incodaca)->where('facu_f_incodfac',$facultad)->orderBy('id','DESC')->get();
        return response()->json([
                'success' => true,
                'notifications' => $notifications
            ], 201); 
        
        
    }
    public function notifications_teachers($facultad)
    {
        $notifications = M_noti_pers::where('tipo_f_incodusu',"=", "2")->where('facu_f_incodfac',$facultad)->orderBy('id','DESC')->get();
        return response()->json([
                'success' => true,
                'notifications' => $notifications
            ], 201); 
        
        
    }
    public function store(M_noti_persRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        foreach ($request->facultades as $value) {
            $facult=M_facu::where('id','=',$value)->first();
            foreach ($request->todos as $tipo) {
                $tipoUser=S_tipo_usua::where('tipo_chnomusu','=',$tipo)->first();
                $m_noti_pers = M_noti_pers::firstOrNew(['id' => $id]);
                $m_noti_pers->fill($request->all());
                $m_noti_pers->facu_f_incodfac=$facult->id;
                $m_noti_pers->tipo_f_incodusu=$tipoUser->id;
                $m_noti_pers->save();
                broadcast(new NotificationStudentEvent($m_noti_pers->title,$m_noti_pers->body,$m_noti_pers->image));
            }
            
        }
       return response()->json([
        'success' => true,
        'message' => ($id)?'Actualizado con éxito':'Se Guardo con éxito',
    ], 201); 
    }
    public function upload(Request $request){
        if ($request->hasFile('file')) {
        $file = $request->file('file');
        $name =  time()."_".$file->getClientOriginalName();
        $file->storeAs('public/uploads/images', $name);         
        return response()->json([
                'success' => true,
                'name' => asset('/storage/uploads/images/'.$name),
                'message' => "Se Subio con exito",
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' =>  __('app.actions.upload.error'),
        ], 201);
    }
    public function destroy($id)//Eliminar
    {
        $m_noti_pers = M_noti_pers::findOrFail($id);
        $m_noti_pers->delete();
     
        return response()->json([
            'success' => true,
            'message' => 'Eliminado con éxito'
        ], 201);
    }
}
