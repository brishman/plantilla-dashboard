<?php

namespace App\Http\Controllers;
use App\Http\Requests\S_seme_acadRequest;
use App\Http\Resources\S_seme_acadCollection;
use App\Http\Resources\S_seme_acadResource;
use App\Models\S_seme_acad;
use Illuminate\Http\Request;

class S_seme_acadController extends Controller
{
    public function index()//Llama al template Blade de laravel
    {
        return view('notificaciones.s_seme_acad.index');
    }

    public function columns()//buscador x campo
    {
        return [
            'id' => 'ID',
            'seme_chnomaca' => 'Nombre'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = S_seme_acad::where($request->column, 'like', "%{$request->value}%")->where('seme_boestaca','!=','0')->orderBy('seme_chnomaca');//para ordenar

        return new S_seme_acadCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new S_seme_acadResource(S_seme_acad::findOrFail($id));
        return $record;
    }

    public function store(S_seme_acadRequest $request){//Guardar y Actualizar
        $id = $request->input('id');
        $s_seme_acad = S_seme_acad::firstOrNew(['id' => $id]);
        $s_seme_acad->fill($request->all());
        $s_seme_acad->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$s_seme_acad
        ];
    }

    public function destroy($id)//Eliminar
    {
        $s_seme_acad = S_seme_acad::findOrFail($id);
        $s_seme_acad->seme_boestaca='0';
        $s_seme_acad->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
