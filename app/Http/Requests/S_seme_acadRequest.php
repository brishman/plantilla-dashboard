<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class S_seme_acadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'seme_chnomaca' =>  ['required'],
            'seme_boestaca' =>  ['required'],                 
          ];
      }
    }
    public function messages()
    {
        return [
            'seme_chnomaca.required'         => 'Es obligatorio.',
            'seme_boestaca.required'         => 'Es obligatorio.',
        ];
    }
}
