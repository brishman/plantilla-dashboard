<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'name'                         => ['required'],
            'email'                        =>  ['required'],
            'new_password'                 =>  ['required'],     
            'tipo_f_incodusu'              =>  ['required'],
            'nive_f_incodniv'              =>  ['required'],
            //'facu_f_incodfac'              =>  ['required'],
           // 'pers_chdocide'              =>  ['required'],
            //'pers_chcelper'              =>  ['required'],
            'pers_chdirper'              =>  ['required'],
          ];
      }
    }
    public function messages()
    {
        return [
            'name.required'                        => 'Es obligatorio.',
            'email.required'                       =>  'Es obligatorio.',
            'new_password.required'                => 'Es obligatorio.',
            'tipo_f_incodusu.required'             => 'Seleccione un item de la lista.',
            'nive_f_incodniv.required'             => 'Seleccione un item de la lista.',
            //'facu_f_incodfac.required'             => 'Seleccione un item de la lista.',
           // 'pers_chdocide.required'               => 'Es obligatorio.',
          //  'pers_chcelper.required'               =>  'Es obligatorio.',
            'pers_chdirper.required'               => 'Es obligatorio.',
        ];
    }
}
