<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class M_noti_persRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules(){
      {
          $id = $this->input('id');
          $rules = [
            'seme_f_incodaca' => 'required',
            'todos' => 'required',
            'title' => 'required',
            'body' => 'required',
            'nive_f_incodniv' => 'required',
        ];
        if ($this->input('selection')=='1') {
          $rules['facultades'] = 'required';
        } 
          if ($this->input('selection')=='2') {
            $rules['facu_f_incodfac'] = 'required';
          } 
        
        return $rules;
         
      }
    }
    public function messages()
    {
        return [
            'seme_f_incodaca.required'    => 'Es obligatorio.',
            'facu_f_incodfac.required'    => 'Es obligatorio.',
            'todos.required'              => 'Es obligatorio.',
            'title.required'              => 'Es obligatorio.',
            'facultades.required'         => 'Es obligatorio.',
            'body.required'               => 'Es obligatorio.',
            'date.required'               => 'Es obligatorio.',
            'nive_f_incodniv.required'    => 'Es obligatorio.',
            
        ];
    }
}
