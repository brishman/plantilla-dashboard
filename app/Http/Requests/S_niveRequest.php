<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class S_niveRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'nive_chnomniv' =>  ['required'],
                          
          ];
      }
    }
    public function messages()
    {
        return [
            'nive_chnomniv.required'  => 'Es obligatorio.',
        ];
    }
}
