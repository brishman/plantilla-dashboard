<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class M_facuRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'facu_chnomfac' =>  ['required'],
            'facu_boestfac' =>  ['required'],                 
          ];
      }
    }
    public function messages()
    {
        return [
            'facu_chnomfac.required'         => 'Es obligatorio.',
            'facu_boestfac.required'         => 'Es obligatorio.',
        ];
    }
}
