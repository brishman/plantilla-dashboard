<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class M_persRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
              'usua_f_incodusu' => ['required'],
              'pers_chnomper'   => ['required'],
              'pers_chapeper'   =>  ['required'],  
              'pers_chdocide'   =>  ['required'],     
              'pers_chcelper'   =>  ['required'],
              'pers_chdirper'   =>  ['required'],    
          ];
      }
    }
    public function messages()
    {
        return [
            'usua_f_incodusu.required'        =>  'Es obligatorio.',
            'pers_chnomper.required'          =>  'Es obligatorio.',
            'pers_chapeper.required'          =>  'Es obligatorio.',
            'pers_chdocide.required'          =>  'Es obligatorio.',
            'pers_chcelper.required'          =>  'Es obligatorio.',
            'pers_chdirper.required'          =>  'Es obligatorio.',
        ];
    }
}
