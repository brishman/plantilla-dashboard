<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class M_usuaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'tipo_f_incodusu' =>  ['required'],
            'nive_f_incodniv' =>  ['required'],  
            'email'           =>  ['required'],  
            'name'            =>  ['required'],
            'pers_chdocide'   =>  ['required'],  
            'pers_chcelper'   =>  ['required'],            
          ];
      }
    }
    public function messages()
    {
        return [
            'tipo_f_incodusu.required' => 'Es obligatorio.',
            'nive_f_incodniv.required' => 'Es obligatorio.',
            'email.required'           => 'Es obligatorio.',
            'name.required'            => 'Es obligatorio.',
            'pers_chdocide.required'   => 'Es obligatorio.',
            'pers_chcelper.required'   => 'Es obligatorio.',
        ];
    }
}
