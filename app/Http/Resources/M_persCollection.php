<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class M_persCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'usua_f_incodusu' => $row->usua_f_incodusu,
                'pers_chnomper' => $row->pers_chnomper,
                'pers_chapeper' => $row->pers_chapeper,
                'pers_chdocide' => $row->pers_chdocide,
                'pers_chcelper' => $row->pers_chcelper,
                'pers_chdirper' => $row->pers_chdirper,
              
            ];
        });
    }
}
