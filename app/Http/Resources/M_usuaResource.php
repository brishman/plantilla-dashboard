<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class M_usuaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'tipo_f_incodusu' => $this->tipo_f_incodusu,
            'nive_f_incodniv' => $this->nive_f_incodniv,
            'facu_f_incodfac' => $this->facu_f_incodfac,
            'usua_chlogusu' => $this->usua_chlogusu,
            'usua_chpasusu' => $this->usua_chpasusu,
            'usua_boestusu' => $this->usua_boestusu,
            
        ];
    }
}
