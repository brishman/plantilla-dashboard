<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class M_usuaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'tipo_f_incodusu' => $row->tipo_f_incodusu,
                'nive_f_incodniv' => $row->nive_f_incodniv,
                'facu_f_incodfac' => $row->facu_f_incodfac,
                'usua_chlogusu'   => $row->usua_chlogusu,
                'usua_chpasusu'   => $row->usua_chpasusu,
                'usua_boestusu'   => $row->usua_boestusu,
              
            ];
        });
    }
}
