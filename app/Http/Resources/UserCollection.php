<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id'            => $row->id,
                'name'          => $row->name,
                'email'         => $row->email,
                'tipo_usuario'  => $row->tipo_usuario,
                'nivel_usuario' => $row->nivel_usuario,
                'facultad'      => $row->facultad,
                'pers_chdocide' => $row->pers_chdocide,
                'pers_chcelper' => $row->pers_chcelper,
                'pers_chdirper' => $row->pers_chdirper,      
            ];
        });
    }
}
