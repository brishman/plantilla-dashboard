<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class M_noti_persCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id'              => $row->id,
                'nive_f_incodniv' => $row->nive_f_incodniv,
                'seme_f_incodaca' => $row->seme_f_incodaca,
                'facu_f_incodfac' => $row->facu_f_incodfac,
                'tipo_f_incodusu' => $row->tipo_f_incodusu,
                'title'           => $row->title,
                'body'            => $row->body,
                'date'            => $row->date,
                'hora'            => \Carbon\Carbon::parse($row->created_at)->format('h:m:s'),
                'image'           => $row->image,
                'facultad'        =>$row->facultad,
                'semestre'        =>$row->semestre,
                'tipo_usuario'    =>$row->tipo_usuario,
                'nivel_usuario'   =>$row->nivel_usuario,
                'number_student'  =>$row->number_student,
                'number_teacher'  =>$row->number_teacher,
            ];
        });
    }
}
