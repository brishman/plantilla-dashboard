<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class S_seme_acadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'seme_chnomaca' => $this->seme_chnomaca,
            'seme_boestaca' => $this->seme_boestaca,
            
        ];
    }
}
