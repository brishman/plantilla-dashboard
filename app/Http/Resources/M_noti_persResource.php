<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class M_noti_persResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'nive_f_incodniv'=> $this->nive_f_incodniv,
            'seme_f_incodaca' => $this->seme_f_incodaca,
            'facu_f_incodfac' => $this->facu_f_incodfac,
            'tipo_f_incodusu' => $this->tipo_f_incodusu,
            'title'           => $this->title,
            'body'            => $this->body,
            'date'            => $this->date,
            'image'           => $this->image,
            'facultad'        =>$this->facultad,
            'semestre'        =>$this->semestre,
            'tipo_usuario'    =>$this->tipo_usuario,
            'nivel_usuario'    =>$this->nivel_usuario,
        ];
    }
}
