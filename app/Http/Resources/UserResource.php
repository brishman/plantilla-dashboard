<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'email'         => $this->email,
            'token'         => $this->token,
            'tipo_usuario'  => $this->tipo_usuario,
            'nivel_usuario' => $this->nivel_usuario,
            'facultad'      => $this->facultad,
            'pers_chdocide' => $this->pers_chdocide,
            'pers_chcelper' => $this->pers_chcelper,
            'pers_chdirper' => $this->pers_chdirper,
            
        ];
    }
}
