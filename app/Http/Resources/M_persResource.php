<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'usua_f_incodusu' => $this->usua_f_incodusu,
            'pers_chnomper' => $this->pers_chnomper,
            'pers_chapeper' => $this->pers_chapeper,
            'pers_chdocide' => $this->pers_chdocide,
            'pers_chcelper' => $this->pers_chcelper,
            'pers_chdirper' => $this->pers_chdirper,
            
        ];
    }
}
