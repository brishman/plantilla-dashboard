<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class M_facuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'facu_chnomfac' => $this->facu_chnomfac,
            'facu_boestfac' => $this->facu_boestfac,
            
        ];
    }
}
