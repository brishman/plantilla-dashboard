<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class S_seme_acadCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'seme_chnomaca' => $row->seme_chnomaca,     
                'seme_boestaca' => $row->seme_boestaca,   
            ];
        });
    }
}
