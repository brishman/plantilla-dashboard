<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class S_tipo_usuaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'tipo_chnomusu' => $this->tipo_chnomusu,
       
            
        ];
    }
}
