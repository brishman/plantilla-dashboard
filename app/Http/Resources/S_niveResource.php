<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class S_niveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'nive_chnomniv' => $this->nive_chnomniv,
       
            
        ];
    }
}
