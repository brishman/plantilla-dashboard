require('./bootstrap');
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Axios from 'axios'
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'

locale.use(lang)
//Vue.use(ElementUI)
Vue.use(ElementUI, {size: 'small'})
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios

Vue.component('x-graph', require('./components/graph/src/Graph.vue').default);
Vue.component('x-graph-line', require('./components/graph/src/GraphLine.vue').default);

Vue.component('user-index', require('./views/users/index.vue').default);
Vue.component('dashboard', require('./views/dashboard/index.vue').default);
Vue.component('perfil-form', require('./views/users/perfil.vue').default);
Vue.component('users-form', require('./views/users/index.vue').default);
Vue.component('m_facu-form', require('./views/m_facu/index.vue').default);
Vue.component('s_nive-form', require('./views/s_nive/index.vue').default);
Vue.component('index-export', require('./views/exports/index.vue').default);
Vue.component('index-recycle', require('./views/recycle/index.vue').default);
Vue.component('s_seme_acad-form', require('./views/s_seme_acad/index.vue').default);
Vue.component('index-notifications', require('./views/notifications/index.vue').default);
Vue.component('count-notifications', require('./views/notifications/count.vue').default);
Vue.component('show-notifications', require('./views/notifications/notifications.vue').default);
Vue.component('x-form-group', require('./components/FormGroup.vue').default);


const app = new Vue({
    el: '#default',
    /*
    mounted:function(){
        Echo.private('notifications')
            .listen('UserSessionChanged', (e) => {
            const notificationElement = document.getElementById('notification');
            const h = this.$createElement;
            this.$message({
                showClose: true,
                message:e.message,
                type: e.type
              });
 
});
  
    }
    */
});
