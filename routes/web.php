<?php
    Auth::routes();
    Route::get('login', 'UserController@showLogin')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::group(['middleware' => ['auth']], function() {
        Route::get('/dashboard', function () {
            return view('home');
        });        
        Route::get('/', function () {
       //         return view('home');
       return redirect('dashboard');
       return redirect()->route('dashboard');
    });
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('register', 'UserController@index')->name('register.index');
    Route::get('user/perfil','UserController@perfil')->name('perfil.index');
    Route::get('user/load_perfil','UserController@loadPerfil');
    Route::get('user/columns','UserController@columns');
    Route::get('user/records','UserController@records');
    Route::get('user/tables','UserController@tables');
    Route::get('user/record/{item}','UserController@record');
    Route::post('user/save_perfil','UserController@updateperfil');
    Route::post('user', 'UserController@store');
    Route::delete('user/baja/{item}','UserController@destroy');
    Route::get('user/assigned/{item}','UserController@assigned');
    //Chat
    Route::get('student/messages/{faculties}/{semesters}','MessagesController@student_messages');
    Route::get('exportar', 'M_noti_persController@exportar')->name('exportar.index');
    Route::get('notifications', 'M_noti_persController@index')->name('notifications.index');
    Route::get('notifications/columns','M_noti_persController@columns');
    Route::get('notifications/records','M_noti_persController@records');
    Route::get('viewrecycle','M_noti_persController@viewrecycle')->name('viewrecycle.index');
    Route::post('notifications/recycle','M_noti_persController@recycle');
    Route::get('notifications/record/{item}','M_noti_persController@record');
    Route::get('notifications/{nive_f_incodniv}/{tipo_f_incodusu}/{facu_f_incodfac}/{seme_f_incodaca}','M_noti_persController@exportpdf');

    Route::get('notifications/viewnotifications','M_noti_persController@countnotificacion');
    Route::get('viewcount','M_noti_persController@viewcountnotifications')->name('viewcount.index');
    Route::post('/user/uploads','UserController@impotarexcel');
    Route::post('notifications/upload','M_noti_persController@upload');
    
    Route::post('notifications', 'M_noti_persController@store');
    Route::delete('notifications/{item}','M_noti_persController@destroy');
    Route::delete('notifications/baja/{item}','M_noti_persController@destroy');
     
    Route::get('faculties', 'M_facuController@index')->name('faculties.index');
    Route::get('faculty/columns','M_facuController@columns');
    Route::get('faculty/records','M_facuController@records');
    Route::get('faculty/record/{item}','M_facuController@record');
    Route::post('faculty', 'M_facuController@store');
    Route::delete('faculty/baja/{item}','M_facuController@destroy');
    //niveles
    Route::get('levels', 'S_niveController@index')->name('levels.index');
    Route::get('level/columns','S_niveController@columns');
    Route::get('level/records','S_niveController@records');
    Route::get('level/record/{item}','S_niveController@record');
    Route::post('level', 'S_niveController@store');
    Route::delete('level/baja/{item}','S_niveController@destroy');
    Route::get('typeuser/records','S_tipo_usuaController@records');
    //Semestres
    Route::get('semesters', 'S_seme_acadController@index')->name('levels.index');
    Route::get('semester/columns','S_seme_acadController@columns');
    Route::get('semester/records','S_seme_acadController@records');
    Route::get('semester/record/{item}','S_seme_acadController@record');
    Route::post('semester', 'S_seme_acadController@store');
    Route::delete('semester/baja/{item}','S_seme_acadController@destroy');
    });
 