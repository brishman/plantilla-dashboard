<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountTable extends Migration
{

    public function up()
    {
        Schema::create('countnotification', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nive_f_incodniv'); //relacion tabla S_TIPO_USUA
            $table->integer('number'); //relacion tabla M_FACU
             $table->timestamps();
             $table->foreign('nive_f_incodniv')->references('id')->on('s_nive'); 
        });
    }
    public function down()
    {
        Schema::dropIfExists('countnotification');
    }
}
