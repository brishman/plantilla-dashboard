<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('usua_boestusu')->default(true);
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('new_password')->nullable();
            $table->text('token')->nullable();
            $table->unsignedInteger('tipo_f_incodusu')->nullable(); //relacion tabla S_TIPO_USUA
            $table->unsignedInteger('nive_f_incodniv')->nullable(); //relacion tabla S_NIVE
            $table->unsignedInteger('facu_f_incodfac')->nullable(); //relacion tabla M_FACU
            $table->string('pers_chdocide')->nullable();
            $table->string('pers_chcelper')->nullable();
            $table->string('pers_chdirper')->nullable();
            $table->timestamps();
            $table->foreign('tipo_f_incodusu')->references('id')->on('s_tipo_usua');
            $table->foreign('nive_f_incodniv')->references('id')->on('s_nive');
            $table->foreign('facu_f_incodfac')->references('id')->on('m_facu');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
