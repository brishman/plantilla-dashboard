<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVOperNotiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_oper_noti', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipo_f_incodusu'); //relacion tabla S_TIPO_USUA
            $table->unsignedInteger('facu_f_incodfac'); //relacion tabla M_FACU
            $table->unsignedInteger('noti_f_incodper'); //relacion tabla M_NOTI_PERS 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_oper_noti');
    }
}
