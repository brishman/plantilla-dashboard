<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMNotiPersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_noti_pers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seme_f_incodaca'); //relacion tabla S_SEME_ACAD  
            $table->unsignedInteger('facu_f_incodfac'); //relacion tabla FACULTAD
            $table->unsignedInteger('tipo_f_incodusu'); //relacion tabla TIPO
            $table->unsignedInteger('nive_f_incodniv'); //relacion tabla TIPO
            $table->string('title');
            $table->text('body');
            $table->integer('number_student')->nullable()->default(0);
            $table->integer('number_teacher')->nullable()->default(0);
            $table->date('date');
            $table->text('image')->nullable();
            $table->timestamps();
            $table->foreign('seme_f_incodaca')->references('id')->on('s_seme_acad');
            $table->foreign('facu_f_incodfac')->references('id')->on('m_facu');
            $table->foreign('tipo_f_incodusu')->references('id')->on('s_tipo_usua');  
            $table->foreign('nive_f_incodniv')->references('id')->on('s_nive');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_noti_pers');
    }
}
