<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMUsuaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_usua', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipo_f_incodusu'); //relacion tabla S_TIPO_USUA
            $table->unsignedInteger('nive_f_incodniv'); //relacion tabla S_NIVE
            $table->unsignedInteger('facu_f_incodfac'); //relacion tabla M_FACU
            $table->string('usua_chlogusu');
            $table->string('usua_chpasusu');
            $table->boolean('usua_boestusu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_usua');
    }
}
