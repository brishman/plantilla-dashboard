<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMPersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_pers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('usua_f_incodusu'); //relacion tabla M_USUA
            $table->string('pers_chnomper');
            $table->string('pers_chapeper');
            $table->string('pers_chdocide');
            $table->string('pers_chcelper');
            $table->string('pers_chdirper');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_pers');
    }
}
