<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        App\Models\S_tipo_usua::create([
            'id' =>'1', 
            'tipo_chnomusu' => 'Alumnos',
        ]);
        App\Models\S_tipo_usua::create([
            'id' =>'2', 
            'tipo_chnomusu' => 'Docentes',
        ]);
        App\Models\S_tipo_usua::create([
            'id' =>'3', 
            'tipo_chnomusu' => 'Administracion',
        ]);
        App\Models\S_nive::create([
            'id' =>'1', 
            'nive_chnomniv' => 'Vicerectorado',
        ]);
        App\Models\S_nive::create([
            'id' =>'2', 
            'nive_chnomniv' => 'Facultad',
        ]);
        App\Models\S_nive::create([
            'id' =>'3', 
            'nive_chnomniv' => 'Standar',
        ]);
        App\Models\S_nive::create([
            'id' =>'4', 
            'nive_chnomniv' => 'Administrador',
        ]);
        App\Models\M_facu::create([
            'id' =>'1', 
            'facu_chnomfac' => 'Todas las Facultades',
            'facu_boestfac' => 1,
        ]);

        App\Models\S_seme_acad::create([
            'id' =>'1', 
            'seme_chnomaca' => '2020 I',
            'seme_boestaca' => 1,
        ]);
        App\Models\S_seme_acad::create([
            'id' =>'2', 
            'seme_chnomaca' => '2020 II',
            'seme_boestaca' => 1,
        ]);

        App\Models\M_count::create([
            'id' =>'1', 
            'nive_f_incodniv' => 1,
            'number' => 0,
        ]);
        App\Models\M_count::create([
            'id' =>'2', 
            'nive_f_incodniv' => 2,
            'number'=> 0
        ]);

        App\Models\User::create([
            'id' =>'1', 
            'name' => 'Adminstrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'tipo_f_incodusu' => '3',
            'nive_f_incodniv' => '4',
            'facu_f_incodfac'=> '1',
            'usua_boestusu'=> true,
            'pers_chdocide'=> '',
            'pers_chcelper'=> '',
            'pers_chdirper'=> '',
            
        ]);

    }
}
